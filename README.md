# Synopsis
A stupid simple ping monitor with failure notification

# Installation
Either clone or download the latest repository snapshot. The script expects the program to live @ `c:\program files\mr-ping`

# Configuration
Modify the included configuration file (`conf\mr-ping.config.json`). Make sure the following values are set:

  - secretKeyId
  - secretAccessKeyID
  - emailFrom
  - emailTo
  - clientName
  - hosts section

# Requirements
  - An active Amazon Web Services Account
  - An IAM user with SES `Send` and `GetQuota` permissions only
  - PowerShell 3

You can verify the version of PowerShell you have installed by opening a PowerShell instance and running the following command
    $psversiontable.psversion

