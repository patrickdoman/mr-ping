$configFile = $env:ProgramFiles + "mr-ping\conf\mr-ping.config.json"
$config = Get-Content -Raw $configFile | ConvertFrom-Json

Add-Type -Path "$env:ProgramFiles\mr-ping\lib\AWSSDK.dll"

$sesClient=[Amazon.AWSClientFactory]::CreateAmazonSimpleEmailServiceClient($config.secretKeyID,$config.secretAccessKeyID)

Function SendAlarm($_host)
{
  $quota = $sesClient.GetSendQuota()
  $quotaRemaining = $quota.GetSendQuotaResult.Max24HourSend - $quota.GetSendQuotaResult.SentLast24Hours
  $emailFrom = $config.emailFrom
  $emailTo = $config.emailTo
  $subject = "ALARM: " + $config.clientName + " - Failed test on node " + $_host.hostName + " - " + $_host.hostAddress
  $body = "<b>Host Name:</b>" + $_host.hostName + "<br /><b>Host Description:</b> " + $_host.hostDescription + "<br /><b>Host Address:</b> " + $_host.hostAddress + "<br /><br /><i>$quotaRemaining of " + $quota.GetSendQuotaResult.Max24HourSend  + " messages left in daily quota</i><br />"

  $request = New-object -TypeName Amazon.SimpleEmail.Model.SendEmailRequest
  $request.Source = $emailFrom
  $list = New-Object 'System.Collections.Generic.List[string]'
  $list.Add($emailTo)
  $list.Add($emailAlsoTo)
  $request.Destination = $list
  $subjectObject = New-Object -TypeName Amazon.SimpleEmail.Model.Content
  $subjectObject.data = $subject
  $bodyContent = New-Object -TypeName Amazon.SimpleEmail.Model.Content
  $bodyContent.data = $body
  $bodyObject = New-Object -TypeName Amazon.SimpleEmail.Model.Body
  $bodyObject.html = $bodyContent
  $message = New-Object -TypeName Amazon.SimpleEmail.Model.Message
  $message.Subject = $subjectObject
  $message.Body = $bodyObject
  $request.Message = $message 

  $response = $sesClient.SendEmail($request)
}

Function TestHosts($config) {
    Foreach($_host in $config.hosts)
    {
      if(!(Test-Connection -Cn $_host.hostAddress -BufferSize 16 -Count 1 -ea 0 -quiet))
      {
        SendAlarm($_host)
      }
    }
}

# main
TestHosts($config)